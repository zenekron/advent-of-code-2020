# Advent of Code 2020

Very questionable solutions to 2020's [Advent of Code][].


## Info

This page is just an empty landing page, if you were looking for a specific language you might want
to check out its dedicated branch, here are some:

  - [Rust][aoc-rust]


[Advent of Code]: https://adventofcode.com/2020/
[aoc-rust]: https://gitlab.com/zenekron/advent-of-code-2020/-/tree/rust
